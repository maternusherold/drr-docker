FROM nvidia/cuda:10.1-cudnn7-devel-ubuntu18.04
WORKDIR /usr/src/app
RUN apt-get update && apt-get install -y --no-install-recommends \
          curl \
	  vim \
          git && \
     rm -rf /var/lib/apt/lists/*

RUN curl -o ~/miniconda.sh -O  https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh  && \
     chmod +x ~/miniconda.sh && \
     ~/miniconda.sh -b -p /opt/conda && \
     rm ~/miniconda.sh
ENV PATH /opt/conda/bin:$PATH

COPY tools ./tools
COPY ct_lymph ./ct_lymph
COPY drr_environment.yml .
RUN conda env create -f drr_environment.yml && conda init bash &&  mkdir /root/drr_output

