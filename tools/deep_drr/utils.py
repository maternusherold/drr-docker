import glob
import math
import numpy as np
import os
import pickle
import PIL.Image as Image
import pydicom

from datetime import datetime
from typing import List, Dict


# TODO: test methods
# TODO: method to set SliceThickness


class Camera:
    def __init__(self, sensor_width, sensor_height, pixel_size,
                 source_to_detector_distance, isocenter_distance):
        self.sensor_width = sensor_width
        self.sensor_height = sensor_height
        self.pixel_size = pixel_size
        self.source_to_detector_distance = source_to_detector_distance
        self.isocenter_distance = isocenter_distance


def clear_directory(path_to_directory: str, file_type: str = '.tiff'):
    """Clears contents of specified directory.

    :param path_to_directory: path to directory with files to delete
    :param file_type: file ending starting with an .
    """
    files = glob.glob(os.path.join(path_to_directory, '*' + file_type))
    for file in files:
        os.remove(file)


def image_saver(images, prefix, save_path):
    """Saves images from given array to specified path.

    To prevent the unintended case of overwriting a file the index per image
    will be added to the file ending.

    :param images: list of images
    :param prefix: image name
    :param save_path: directory to save to
    """
    for i in range(0, images.shape[0]):
        image_pil = Image.fromarray(images[i, :, :])
        # add the index to prevent unintended overwriting
        image_pil.save(os.path.join(save_path, '_'.join([prefix, str(i), '.tiff'])))


def param_saver(thetas: np.array, phis: np.arange, origin: list, prefix: str,
                save_path: str, proj_mats: np.array = None, camera: Camera = None,
                photons: float = None, spectrum: np.array = None,
                narrow_scope: bool = True) -> bool:
    """Saves parameter settings to provided directory.

    :param thetas: list of thetas used for generation; paired with phis
    :param phis: list of phis used for generation; paired with thetas
    :param origin: origin used
    :param prefix: file name
    :param save_path: directory to save file to
    :param proj_mats: projection matrices used
    :param camera: camera object used
    :param photons: photon count used
    :param spectrum: spectrum used
    :param narrow_scope: bool how much meta data to include; narrow: timestamp,
                thetas, phis, origin; full: narrow plus objects of projection
                matrices, camera as well as photon count, spectrum and IO
    :return: status indicator of saving
    """
    if narrow_scope:
        data = {'date': datetime.now(), 'thetas': thetas,
                'phis': phis, 'origin': origin}
    else:
        i0 = np.sum(spectrum[:, 0] * (spectrum[:, 1] /
                                      np.sum(spectrum[:, 1]))) / 1000
        data = {'date': datetime.now(), 'thetas': thetas,
                'phis': phis, 'proj_mats': proj_mats,
                'camera': camera, 'origin': origin,
                'photons': photons, 'spectrum': spectrum, 'I0': i0}

    with open(os.path.join(save_path, prefix + '.pickle'), 'wb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

    return True


def save_images(images: list, image_attributes: np.array, missing_attr: str):
    """Sets new image attributes and saves the images.

    :param images: sorted list of image paths
    :param image_attributes: according to images list of new attributes
    :param missing_attr: str of missing attr
    """
    for ds in dicom_image_gen(images):
        # TODO: set new attr
        # ...
        # TODO: save updated image
        # ds.save_as(image)
        pass


def add_attribute_to_dcm(path_to_images: str, missing_attr: str, method,
                         file_type: str = 'dcm'):
    """Adding the missing attribute to the provided DICOM images using method.

    As some data sets might miss relevant attributes for computing DRRs on
    them those attributes shall be added afterwards. The attributes are computed
    using the callable provided via method.

    :param path_to_images: path to the images as string without the file ending
    :param missing_attr: attr specification by DICOM keyword - see DICOM documentation
    for clarification
    :param method: callable to compute missing attributes; returns list of attr values
    :param file_type: ending of image file type
    """
    images = get_paths_to_images(path_to_images, file_type)

    # raise error if no matching files in provided dir
    if not images:
        raise ValueError('No files to change in provided '
                         'directory: {}'.format(path_to_images))

    image_attributes = method(images, missing_attr)
    save_images(images, image_attributes, missing_attr)


def get_paths_to_images(path_to_images: str, file_type: str = 'dcm') -> list:
    """Returns path to images in a volume.

    :param path_to_images: path to the images as string without the file ending
    :param file_type: ending of image file type
    # TODO: returns
    """
    # remove trailing path separator in case a poorly formatted path was passed
    path_to_images = remove_ending_character(path_to_images, os.path.sep)
    # only have file ending
    file_type = remove_starting_character(file_type, '.')
    images = sorted(glob.glob(
        os.path.abspath(
            os.path.join(path_to_images, '*.' + file_type))))

    return images


def find_slice_thickness(images: list, max_search_count: int = None,
                         default_thickness: float = 1.0) -> float:
    """Searches all or up to number of images for the attr SliceThickness.

    :param images: sorted list of image paths
    :param max_search_count: max. number of images to check before stopping
    :param default_thickness: default when thickness is not found
    :return: found thickness or default value
    """
    thickness = default_thickness

    if max_search_count is None or max_search_count >= len(images):
        max_search_count = len(images)

    for ds in dicom_image_gen(images[:max_search_count]):
        if check_for_attr(file=ds, attr='SliceThickness'):
            thickness = ds.SliceThickness

    return thickness


def count_slice_location(images: list, **kwargs) -> np.array:
    """Computes image/slice location by assigning a fixed value per slice
    counting from the center.images

    As per definition slice location is a DICOM attr defining the relative
    position of the image plane expressed in mm.
    To approximate the slice location the slice thickness is taken an then added
    per slice as distance from the center. If no slice thickness is given, 1mm
    will be used.
    :param images: sorted list of image paths
    :return: list of slice location per image
    """
    slice_center = math.ceil(len(images)/2-1)
    slice_locations = np.zeros(len(images))
    slice_thickness = find_slice_thickness(images)

    # there will be slice_center slices before and
    #   len(images)-slice_center-1 slices after the center
    min_thickness = -1 * slice_center * slice_thickness
    max_thickness = (len(images) - slice_center) * slice_thickness

    for image_index, _ in enumerate(images):
        slice_locations[image_index] = min_thickness - image_index * int(slice_thickness)

    if slice_locations[-1] != max_thickness:
        raise ValueError('maximum slice thickness not as predicted. '
                         'got {} for {}'.format(slice_locations[-1], max_thickness))

    return slice_locations


def remove_ending_character(file_descriptor: str, char_to_remove: chr) -> str:
    """Removes an unwanted character at the end of an file descriptor.

    :param file_descriptor: string
    :param char_to_remove: character to be removed
    :return: cleaned file_descriptor
    """
    if file_descriptor[-1] == char_to_remove:
        file_descriptor = file_descriptor[:-1]
    return file_descriptor


def remove_starting_character(file_descriptor: str, char_to_remove: chr) -> str:
    """Removes an unwanted character at the beginning of an file descriptor.

    :param file_descriptor: string
    :param char_to_remove: character to be removed
    :return: cleaned file_descriptor
    """
    if file_descriptor[0] == char_to_remove:
        file_descriptor = file_descriptor[1:]
    return file_descriptor


def open_dicom_image(image) -> pydicom.dataset.FileDataset:
    """Opens and returns dicom image.

    :param image: path to dicom image
    :return: dicom image
    """
    return pydicom.dcmread(image)


def dicom_image_gen(paths_to_files: List[str]) -> pydicom.dataset.FileDataset:
    """Yields next dicom image in path.

    Generator for faster and lightweight access to dicom images in a given
    directory.

    :param paths_to_files: list of paths to images
    """
    for dcm in paths_to_files:
        yield open_dicom_image(dcm)


def check_for_attr(file: pydicom.dataset.FileDataset, attr: str) -> bool:
    """Checks a file's attributed if a query attribute is contained.

    :param file: dicom dataset to be examined
    :param attr: attribute to search for
    :return: whether dicom dataset contains attribute
    """
    return hasattr(file, attr)


def search_images_for_attr(images: list, search_attributes: List[str]) -> dict:
    """Searches given list of images for an attribute and returns indicators for presence.

    :param images: sorted list of image paths
    :param search_attributes: list of attributes to search for
    :return: dict of images and indicator of attr presence
    """
    res = dict()
    res['searched_attributes'] = search_attributes
    for ds in dicom_image_gen(paths_to_files=images):
        res[ds.InstanceNumber] = [1 if check_for_attr(ds, attr)
                                  else 0 for attr in search_attributes]
    return res


def get_attributes_for_images(images: list, search_attributes: List[str]) -> dict:
    """Searches given list of images for an attribute and returns found values.

    :param images: sorted list of image paths
    :param search_attributes: list of attributes to search for
    :return: dict of files and found values
    """
    res = dict()
    res['searched_attributes'] = search_attributes
    for ds in dicom_image_gen(paths_to_files=images):
        res[ds.InstanceNumber] = np.array([ds.get(attr) if check_for_attr(ds, attr)
                                           else np.nan for attr in search_attributes])
    return res


def get_attribute_summary(attr_list: List[str],
                          attr_summary: Dict[str, List[int]]) -> dict:
    """Calculates a quantitative summary for searched attributes.

    :param attr_list: list of attributes searched for
    :param attr_summary: quantitative summary of present attributes
    """
    res = dict()
    res['slice_count'] = len(attr_summary)
    print(attr_summary)
    summation = np.nansum(np.array(list(attr_summary.values())[1:]), axis=0)
    for i, attr in enumerate(attr_list):
        res[attr] = summation[i]
    return res


def check_paths(paths: List[str]):
    """Checks if path exists.

    :param paths: paths to file, directory or similar to check
    """
    for path in paths:
        if not os.path.exists(os.path.abspath(path)):
            raise ValueError(f'{path} cannot be resolved.')


def search_volumes_for_attr(volumes: list, search_attributes: List[str],
                            file_type: str = '.dcm') -> dict:
    """Searches volumes for an attribute and returns summary on it.

    :param volumes: list of volume paths to search
    :param search_attributes: list of attributes to search for
    :param file_type: file type of images
    :return: summary of search
    """
    check_paths(volumes)
    summary_volumes = {}

    # volume is the path to a directory of CT volumes
    for volume in volumes:
        # list of image paths for slices current volume
        images = get_paths_to_images(volume, file_type=file_type)
        # search all slices for current volume for attributes
        try:
            summary_volumes[volume] = search_images_for_attr(images=images,
                                                             search_attributes=search_attributes)
        except AttributeError as attr_err:
            print(f'\nan AttributeError occurred: {attr_err}')
            print('check if all slices do have the needed attributes with '
                  'get_attribute_summary()\n')
            raise

    return summary_volumes


def print_info(info: str):
    """Prints provided info in a special format.

    :param info: information to print as string
    """
    print()
    print(f'[INFO] '.ljust(80, '='))
    print(info)
    print(''.ljust(80, '='))
