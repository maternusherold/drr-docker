# DRR Docker

Providing a Dockerfile for an easy deployment of the DRR-pipeline described in my thesis.

## Usage

### Running an instance

1. clone the repository
2. build the image by running `docker build -t drr-docker .`
3. execute a container by running `docker run -it --runtime=nvidia --mount source=output,target=/root/drr_output drr-docker /bin/bash`. This will create an instance of the before created image and log in to the shell.
4. activate the conda environment by running `conda activate carm`
5. export the pythonpath by running `export PYTHONPATH="$PWD"`
6. execute the `DataGenerator` by running `python tools/deep_drr/data_generator.py` - if needed configure the parameters!

The outputs will be saved to the newly created volume. Usually under `/var/lib/docker/volumes/<volume-name>/_data`

### Inspecting the produced images

The easiest way to inspect the images is by either loading them into a Jupyter Notebook or displaying them from inside a Python prompt. Either way, the commands roughly are:

```python
import matplotlib.pyplot as plt
import numpy as np

from PIL import Image

image = Image.open(<path>)  # load image 
image = np.array(image)     # for better compuation
image = np.log10(image)     # for better contrast
image = image / 255.        # normalization
plt.imshow(image, cmap='gray')
plt.show()
```
