import numpy as np
import os
from typing import List, Tuple


class ProjMatrix:
    def __init__(self, R, K, t):
        self.R = np.array(R, dtype=np.float32)
        self.t = np.array(t, dtype=np.float32)
        self.K = np.array(K, dtype=np.float32)
        self.P = np.matmul(self.K, np.concatenate(
            (self.R, np.expand_dims(self.t, 1)), axis=1))
        self.rtk_inv = np.matmul(np.transpose(self.R), np.linalg.inv(self.K))

    def get_rtk_inv(self):
        return self.rtk_inv

    def get_projection(self):
        return self.P

    def get_camera_ceter(self):
        return -np.matmul(np.transpose(self.R),self.t)

    def get_principle_axis(self):
        axis = self.R[2, :]/self.K[2, 2]
        return axis

    def get_conanical_proj_matrix(self, voxel_size, volume_size, origin_shift):
        inv_voxel_scale = np.zeros([3, 3])
        inv_voxel_scale[0][0] = 1 / voxel_size[0]
        inv_voxel_scale[1][1] = 1 / voxel_size[1]
        inv_voxel_scale[2][2] = 1 / voxel_size[2]
        inv_ar = np.matmul(inv_voxel_scale, self.rtk_inv)

        source_point = np.zeros((3, 1), dtype=np.float32)
        camera_ceter = - self.get_camera_ceter()
        source_point[0] = -(-0.5 * (volume_size[0] - 1.0) +
                            origin_shift[0] * inv_voxel_scale[0, 0] +
                            inv_voxel_scale[0, 0] * camera_ceter[0])
        source_point[1] = -(-0.5 * (volume_size[1] - 1.0) +
                            origin_shift[1] * inv_voxel_scale[1, 1] +
                            inv_voxel_scale[1, 1] * camera_ceter[1])
        source_point[2] = -(-0.5 * (volume_size[2] - 1.0) +
                            origin_shift[2] * inv_voxel_scale[2, 2] +
                            inv_voxel_scale[2, 2] * camera_ceter[2])
        return inv_ar, source_point


def generate_projection_matrices_from_values(source_to_detector_distance,
                                             pixel_dim_x, pixel_dim_y,
                                             sensor_size_x, sensor_size_y,
                                             isocenter_distance, 
                                             phi_list: List[float],
                                             theta_list: List[float], 
                                             rho_list: List[float] = None,
                                             offset_list: List[float] = None) \
        -> List[ProjMatrix]:
    """Generating a projection matrix for each angle setting/viewing angle.

    The settings are defined as the cartesian product of all theta and phi angles
    provided. Therefore, the provided lists of angles contain redundancy.
    This means, if there are 3 thetas and 3 phis a total of 9 projection matrices
    will be computed.

    TODO: specify parameters
    :param source_to_detector_distance: distance between source and detector
    :param pixel_dim_x: 
    :param pixel_dim_y: 
    :param sensor_size_x: 
    :param sensor_size_y: 
    :param isocenter_distance: 
    :param phi_list: rotation angles along objects  inferior-superior axis
                    in radians (roll)
    :param theta_list: rotation angles along objects right-left axis in radians (pitch)
    :param rho_list: rotation angles along objects posterior-anterior axis
                    in radians (yaw); must be same length as phi_list and theta_list
    :param offset_list: list of linear translation values;
                    must be same length as phi_list and theta_list 
    """
    number_of_projections = phi_list.__len__()
    matrices = []
    print(f"generating {number_of_projections} matrices")

    # rho is rotation a long the detector's principle axis - third axis in system 
    if not rho_list:
        rho_list = np.zeros(number_of_projections)
    if not offset_list:
        # 3D offset for all projections are 0 - no translation 
        offset_list = np.zeros((number_of_projections, 3))

    # TODO: what's done here?
    K = generate_camera_intrinsics(source_to_detector_distance, pixel_dim_x,
                                   pixel_dim_y, sensor_size_x, sensor_size_y)

    # generate projection matrix per combination of phi and theta 
    for phi, theta, rho, offset in zip(phi_list, theta_list, rho_list, offset_list):
        R = generat_rotation_from_angles(phi, theta, rho)
        t = generate_translation(isocenter_distance, offset[0], offset[1], offset[2])
        matrices.append(ProjMatrix(R, K, t))

    return matrices


def generate_camera_intrinsics(source_to_detector_distance, pixel_dim_x,
                               pixel_dim_y, sensor_size_x, sensor_size_y):
    K = np.zeros([3, 3])
    K[0, 0] = source_to_detector_distance / pixel_dim_x
    K[1, 1] = source_to_detector_distance / pixel_dim_y
    K[0, 2] = sensor_size_x / 2
    K[1, 2] = sensor_size_y / 2
    K[2, 2] = 1.0
    return K


def generate_translation(isocenter_distance, offset_x: float = 0,
                         offset_y: float = 0, offset_z: float = 0):
    """Generates values for linear translation. 
    
    TODO: clarify isocenter_distance
    :param isocenter_distance: height of the scanner
    :param offset_x: translation in x direction - RL
    :param offset_y: translation in y direction - SI
    :param offset_z: translation in z direction - AP
    :return: 4dim list for linear translation 
    """
    t = np.array([offset_x, offset_y, isocenter_distance + offset_z])
    return t


def generat_rotation_from_angles(phi: float, theta: float,
                                 rho: float = 0) -> np.array:
    """Calculates rotation matrix around all three axis.

    Based on the rotation angles in radians the rotation matrix for all three
    axis will be computed. The parameter corresponding axes are defined based
    on a right hand coordinate system.

    :param phi: rotation angle in radians around SI aka y-axis - roll
    :param theta: rotation angle in radians around RL aka x-axis - pitch
    :param rho: rotation angle in radians around AP aka z-axis - yaw
    :return: rotations matrix
    """
    # rotation around phi and theta
    sin_p = np.sin(phi)
    neg_cos_p = -np.cos(phi)
    z = 0
    sin_t = np.sin(theta)
    cos_t = np.cos(theta)
    omc = 1-cos_t

    # TODO: what kind of rotation ?
    R = np.array([[sin_p*sin_p*omc + cos_t, sin_p*neg_cos_p*omc -
                   z*sin_t, sin_p*z*omc + neg_cos_p*sin_t],
                  [sin_p*neg_cos_p*omc + z*sin_t, neg_cos_p*neg_cos_p*omc +
                   cos_t,   neg_cos_p*z*omc - sin_p*sin_t],
                  [sin_p*z*omc - neg_cos_p*sin_t, neg_cos_p*z*omc +
                   sin_p*sin_t, z*z*omc + cos_t]])

    # rotation matrix around detector principle axis - AP axis equalling yaw
    rho = -phi + np.pi * 0.5 + rho
    R_principle = np.array([[np.cos(rho), -np.sin(rho), 0],
                            [np.sin(rho), np.cos(rho), 0], [0, 0, 1]])
    R = np.matmul(R_principle, R)

    return R


def read_matrices_from_file(path, lim=100000000):
    R_in = open(os.path.join(path,"R.txt")).read().split("\n")
    t_in = open(os.path.join(path, "T.txt")).read().split("\n")
    K_in = open(os.path.join(path, "K.txt")).read().split("\n")
    projs = []
    for i in range(R_in.__len__()):
        if R_in[i] == "" or i > lim:
            return projs
        R = np.array(list(map(float, R_in[i].split(" ")[0:9])))
        R.shape = (3, 3)
        K = np.array(list(map(float, K_in[i].split(" ")[0:9])))
        K.shape = (3, 3)
        t = np.array(list(map(float, t_in[i].split(" ")[0:3])))
        t.shape = (3)
        projs.append(ProjMatrix(R, K, t))
    return projs


def generate_uniform_angels(min_theta: float, max_theta: float, min_phi: float,
                            max_phi: float, spacing_theta: float, spacing_phi: float) \
        -> Tuple[np.array, np.array]:
    """Computes lists of thetas and phis in radians.

    The lists are computed in a fashion that it takes all possible combinations
    into account. Therefore, the computed thetas and phis are repeated to match
    each of the computed phis and thetas respectively.

    # TODO: check which is which
    #   idea:
    #       - RL corresponds to x axis
    #       - SI corresponds to y axis
    #       - AP corresponds to z axis
    # TODO: polar angle elevation or inclination angle?
    :param min_theta: minimum azimuth or polar angle
    :param max_theta: maximum azimuth or polar angle
    :param min_phi: minimum azimuth or polar angle
    :param max_phi: maximum azimuth or polar angle
    :param spacing_theta: spacing between angles
    :param spacing_phi: spacing between angles
    :return: lists of thetas and phis as radians
    """
    thetas = np.array(np.arange(min_theta, max_theta + spacing_theta / 2,
                                step=spacing_theta)) / 180 * np.pi
    phis = np.array(np.arange(min_phi, max_phi + 1, step=spacing_phi)) / 180 * np.pi

    thetas_cartesian = np.tile(thetas, phis.__len__())
    phis_cartesian = phis.repeat(thetas.__len__(), 0)

    return thetas_cartesian, phis_cartesian
