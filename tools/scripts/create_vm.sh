export IMAGE_FAMILY="tf-latest-gpu"
export ZONE="us-central1-a"
export INSTANCE_NAME="carm-position-prediction"

gcloud compute instances create $INSTANCE_NAME \
	  --zone=$ZONE \
	  --image-family=$IMAGE_FAMILY \
	  --image-project=deeplearning-platform-release \
	  --custom-cpu=4 \
	  --custom-memory=16GB \
	  --create-disk="size=250G" \
	  --maintenance-policy=TERMINATE \
	  --accelerator="type=nvidia-tesla-p4,count=1" \
	  --metadata="install-nvidia-driver=True"

