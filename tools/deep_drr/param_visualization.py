"""
Generating images based on parameter settings.

To sanity check the predictions a model makes the images, a model pretends to
see - i.e. predicts - shall be generated and compared to the actual images the
model saw.

Intended use case:
* train model on data
* get predictions on data - predictions are 6D parameters
* generate an image from the predicted settings - the image the model pretends to see
"""

import numpy as np
import pandas as pd

from deep_drr.data_generator import DataGenerator
from deep_drr.utils import Camera


class ParameterVisualizer:

    def __init__(self, parameter_table: pd.DataFrame, volume_path: str,
                 output_dir: str):
        """Utilizes the DataGenerator's functionality to generate an image
        from the provided settings.

        :param parameter_table: 6D x-ray settings
        :param volume_path: path to the ct-volume
        :param output_dir: path to save the images to
        """
        self.parameters = parameter_table
        self.output_dir = output_dir
        self.generator = DataGenerator(theta_range=(0, 0), phi_range=(0, 0),
                                       origin_x_range=(0, 0), origin_y_range=(0, 0),
                                       origin_z_range=(0, 0), ct_volume_path=volume_path,
                                       save_to_dir=output_dir)
        self.generator.data_table = self.parameters
        self.generator.load_ct_volume(fall_back_slice_thickness=1.0)

    def create_images_from_params(self):
        """Creates images based on settings. """
        # 4x4 binning
        camera_44 = Camera(sensor_width=620, sensor_height=480, pixel_size=0.62,
                           source_to_detector_distance=1000, isocenter_distance=750)

        self.generator.camera = camera_44
        self.generator.create_images_from_settings()


if __name__ == '__main__':

    data = np.array([[90, 95, 0, -40, 100, 80]])
    col_names = ['theta', 'phi', 'rho', 'origin_x', 'origin_y', 'origin_z']
    params = pd.DataFrame(data=data, columns=col_names)
    params['file_names'] = 'affe'
    print(params.dtypes)

    volume_path = '/home/maternusherold/carm/data/ct_lymph/ct_lymph_003'
    output_path = '/home/maternusherold/carm/data/reverse_generated_images'

    visualizer = ParameterVisualizer(parameter_table=params,
                                     volume_path=volume_path, output_dir=output_path)
    visualizer.create_images_from_params()
